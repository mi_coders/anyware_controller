package ch.harenberg.anyware.web.boundary;

import ch.harenberg.anyware.entity.Tenant;
import ch.harenberg.anyware.remoteSystems.RestClientForAnyware;
import ch.harenberg.anyware.remoteSystems.RestClientForSFDC;
import ch.harenberg.anyware.web.boundary.util.JsfUtil;
import ch.harenberg.anyware.web.boundary.util.PaginationHelper;
import ch.harenberg.anyware.web.session.TenantFacade;
import java.io.IOException;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import org.json.simple.parser.ParseException;

@Named("tenantController")
@SessionScoped
public class TenantController implements Serializable {

    private Tenant current;
    private DataModel items = null;
    @EJB
    private ch.harenberg.anyware.web.session.TenantFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public TenantController() {
    }

    public Tenant getSelected() {
        if (current == null) {
            current = new Tenant();
            selectedItemIndex = -1;
        }
        return current;
    }

    private TenantFacade getFacade() {
        return ejbFacade;
    }
    
    public List<Tenant> getTenantsList(){
        return getFacade().findAll();
        
    }

    public String getContactedDate() {
        if (current.getTrialReminderDate() == null) {
            return ResourceBundle.getBundle("/Bundle").getString("btnRecontact_Label");
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            return formatter.format(current.getTrialReminderDate());
        }
    }
    
    public String getInviteSentDate() {
        if (current.getTrialReminderDate() == null) {
            return ResourceBundle.getBundle("/Bundle").getString("btnInviteSent_Label");
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            return formatter.format(current.getTrialReminderDate());
        }
    }

    public String updateContactedDate() {
        Calendar cal = Calendar.getInstance();
        current.setTrialReminderDate(cal.getTime());
        getFacade().edit(current);

            recreateModel();
        return "List";
    }
    
    public String updateInvitedDate() {
        Calendar cal = Calendar.getInstance();
        current.setInvitationMailSentDate(cal.getTime());
        getFacade().edit(current);

            recreateModel();
        return "List";
    }
    
    public String initializeTenant() {
        try {
            getFacade().edit(current);
            current.setSCSAdminMail(current.getBsk() + ".mds@swisscom.com");
            RestClientForAnyware anyc = new RestClientForAnyware();
            current = anyc.createTenant(current);
            anyc.applyLicence(current);
            anyc.acceptEulaForTenant(current);
            Integer tenantAdminId = anyc.createTenantSuperUser(current);
            Integer firstRoleId = anyc.getFirstTenantRole(current);
            anyc.assignCustomerAdminRoles(current, tenantAdminId, firstRoleId);
            if(current.getIsTrial()){
                Calendar cal = Calendar.getInstance();
                current.setTrialStartDate(cal.getTime());
                cal.add(Calendar.DAY_OF_MONTH, 30);
                current.setTrialEndDate(cal.getTime());
            }
            getFacade().edit(current);
            JsfUtil.addSuccessMessage("Tenant Created");
            recreateModel();
            return "List";
            
        } catch (IOException ex) {
            Logger.getLogger(TenantController.class.getName()).log(Level.SEVERE, null, ex);
            JsfUtil.addErrorMessage(ex,"Error, check");
            return "List";
        } catch (ParseException ex) {
            Logger.getLogger(TenantController.class.getName()).log(Level.SEVERE, null, ex);
            JsfUtil.addErrorMessage(ex, "Error, check");
            return "List";
        }
    }
    

    public String getLicense(){
        ch.harenberg.anyware.boundary.tenantController tc = new ch.harenberg.anyware.boundary.tenantController();
        tc.updateTenantLicences();
        return "List";
    }
    
    public String orderLicence() {
        try {
            RestClientForSFDC sfdc = new ch.harenberg.anyware.remoteSystems.RestClientForSFDC();
            if (current.getLicenceKey() == null) {
                //no existing license, first order
                current.setLicenceID(sfdc.createOperatorOrderForNewLicense(current));
                current.setLicenceKey("Pending");
                getFacade().edit(current);
                JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("LicenseOrderSuccess"));
            } else {
                //licence exists, updating old licence
                current.setLicenceID(sfdc.createOperatorOrderForUpdateLicense(current));
                current.setLicenceKey("Pending");
                getFacade().edit(current);
                JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("LicenseOrderSuccess"));
            }

            recreateModel();
            return "List";
        } catch (IOException ex) {
            Logger.getLogger(TenantController.class.getName()).log(Level.SEVERE, null, ex);
            JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("LicenseOrderError"));
            return null;
        } catch (Exception ex) {
            Logger.getLogger(TenantController.class.getName()).log(Level.SEVERE, null, ex);
            JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("LicenseOrderError"));
            return null;
        }

    }

    public String revokeLicense() {
        try {
            RestClientForSFDC sfdc = new ch.harenberg.anyware.remoteSystems.RestClientForSFDC();
            sfdc.revokeCustomerLicence(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("LicenseRevokeSuccess"));
            current.setLicenceKey("Revoked");
            getFacade().edit(current);

            recreateModel();
            return "List";
        } catch (IOException ex) {
            Logger.getLogger(TenantController.class.getName()).log(Level.SEVERE, null, ex);
            JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("LicenseRevokeError"));
            return null;
        } catch (Exception ex) {
            Logger.getLogger(TenantController.class.getName()).log(Level.SEVERE, null, ex);
            JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("LicenseRevokeError"));
            return null;
        }
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Tenant) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Tenant();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("TenantCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Tenant) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("TenantUpdated"));
            recreateModel();
            return "List";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Tenant) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        if (current.getLicenceKey() == null) {
            performDestroy();
            recreatePagination();
            recreateModel();
            return "List";
        } else {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle("/Bundle").getString("TenantCannotBeDeleted"));
            recreatePagination();
            recreateModel();
            return "List";
        }
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("TenantDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Tenant getTenant(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Tenant.class)
    public static class TenantControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TenantController controller = (TenantController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "tenantController");
            return controller.getTenant(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Tenant) {
                Tenant o = (Tenant) object;
                return getStringKey(o.getIdtenant());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Tenant.class.getName());
            }
        }

    }

}
