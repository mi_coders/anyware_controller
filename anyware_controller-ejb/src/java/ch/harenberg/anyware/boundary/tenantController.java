/*
 * Copyright (C) 2014 Frank Harenberg frank.harenberg@swisscom.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.harenberg.anyware.boundary;

import ch.harenberg.anyware.entity.Tenant;
import ch.harenberg.anyware.remoteSystems.RestClientForAnyware;
import ch.harenberg.anyware.remoteSystems.RestClientForSFDC;
import ch.harenberg.anyware.session.TenantFacade;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.NoResultException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Frank Harenberg <frank@harenberg.ch>
 *
 * tenantController class contains the logic that will manipulate a tenant on
 * the anyware platform
 */
public class tenantController {

    TenantFacade tenantFacade = lookupTenantFacadeBean();

    /**
     * updateTenantLicences
     * <p>
     * The method will retrieve all tenant objects from database which have a
     * license key on status pending<br>
     * For retrieved tenants, it will check with the salesforce portal whether a
     * license has been issued<br>
     * issued licenses are applied to existing tenants, resp. a tenant is
     * created and initialized if no previous tenant is known.
     */
    public void updateTenantLicences() {
        Collection tenants = tenantFacade.initializeTenants();
        if (tenants.isEmpty()) {
            Logger.getLogger(tenantController.class.getName()).log(Level.INFO, "No tenants with pending licence request");
        } else {
            try {
                RestClientForSFDC sfdc = new RestClientForSFDC();
                Iterator it = tenants.iterator();
                while (it.hasNext()) {
                    Object element = it.next();
                    if (element instanceof Tenant) {
                        Tenant current = (Tenant) element;
                        String licenceStatus = "";
                        try {
                            licenceStatus = sfdc.getOperatorOrderById(current);
                        } catch (Exception ex) {
                            Logger.getLogger(tenantController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        if (!licenceStatus.equals("Pending")) {
                            //Yeai, we got a licence key!
                            Logger.getLogger(tenantController.class.getName()).log(Level.INFO, "License Key for tenant received", new Object[]{(String) current.getBsk(), licenceStatus});
                            if (current.getIsTrial()) {
                                //if it is a trial, we set the trial period
                                Calendar cal = Calendar.getInstance();
                                current.setTrialStartDate(cal.getTime());
                                cal.add(Calendar.DAY_OF_MONTH, 30);
                                current.setTrialEndDate(cal.getTime());
                            }
                            current.setLicenceKey(licenceStatus);

                            lookupTenantFacadeBean().edit(current);
                            RestClientForAnyware anyc = new RestClientForAnyware();

                            //If no Tenant Id exists, we first have to create a tenant
                            if (current.getTenantId() == null) {
                                current = this.initializeTenantOnPlatform(current, anyc);
                                lookupTenantFacadeBean().edit(current);
                            } else {
                                anyc.applyLicence(current);
                            }
                        }
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(tenantController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(tenantController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * initializeTenantOnPlatform
     * <p>
     * The method will <ol>
     * <li>set a standardized uid for the admin user
     * <code>$BSK$.mds@swisscom.com</code></li>
     * <li>create the tenant with platform admin user</li>
     * <li>apply the retrieved license</li>
     * <li>with SCS tenant admin user accept eula in name of customer</li>
     * <li>with SCS tenant admin user create a user intended for real customer
     * admin (customer admin user)</li>
     * <li>with SCS tenant admin user assign the customer admin user all roles
     * available in tenant</li>
     * </ol>
     *
     * @param current ch.harenberg.anyware.entitiy.tenant, the current tenant to
     * be initialized on anyware platform
     * @param anyc ch.harenberg.anyware.remoteSystems.RestClientForAnyware the
     * initialized AnyWareRestClient
     * @return the updated tenant after creation on anyware platform with set
     * attributes tenantid, CMPartitionID and DM ParitionID
     *
     */
    private Tenant initializeTenantOnPlatform(Tenant current, RestClientForAnyware anyc) {
        //setting users, passwords are created in the database by before insert trigger
        current.setSCSAdminMail(current.getBsk() + ".mds@swisscom.com");
        try {
            current = anyc.createTenant(current);
            anyc.applyLicence(current);
            anyc.acceptEulaForTenant(current);
            Integer tenantAdminId = anyc.createTenantSuperUser(current);
            Integer firstRoleId = anyc.getFirstTenantRole(current);
            anyc.assignCustomerAdminRoles(current, tenantAdminId, firstRoleId);
            return current;
        } catch (IOException ex) {
            Logger.getLogger(tenantController.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(tenantController.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * gets the TenantFacade for retrieving / persisting tenant objects
     *
     * @return TenantFacade
     */
    private TenantFacade lookupTenantFacadeBean() {
        try {
            Context c = new InitialContext();
            return (TenantFacade) c.lookup("java:global/anyware_controller/anyware_controller-ejb/TenantFacade!ch.harenberg.anyware.session.TenantFacade");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    /**
     * retrieves the platform report and parses it. For every tenant where a
     * corresponding tenantid is found in the database, the values active
     * Devices, active Users, total devices and whether the MDM Certificate has
     * been applied are updated in the database for further export.
     */
    public void updateSiteReport() {
        RestClientForAnyware anyc = new RestClientForAnyware();
        JSONArray ary = anyc.getPlatformReport();
        for (int i = 0; i < ary.size(); i++) {
            JSONObject res = (JSONObject) ary.get(i);
            try {
                Tenant tenant = this.lookupTenantFacadeBean().findByTenantId((int) (long) res.get("tenant_id"));
                if (tenant != null) {
                    tenant.setActiveUsers((int) (long) res.get("active_users"));
                    tenant.setDeviceCount((int) (long) res.get("device_count"));
                    tenant.setDeviceCountActive((int) (long) res.get("device_count_active"));
                    tenant.setMdmCertificateUploaded((Boolean) res.get("mdm_certificate_uploaded"));
                    this.lookupTenantFacadeBean().edit(tenant);
                }
            } catch (javax.persistence.NoResultException ex) {
                Logger.getLogger(getClass().getName()).log(Level.INFO, "tenant information missing in database", ex);
            }
        }

    }
}
