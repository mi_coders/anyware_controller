/*
 * Copyright (C) 2014 Frank Harenberg frank@harenberg.ch
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ch.harenberg.anyware.timer;

import ch.harenberg.anyware.boundary.tenantController;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;

/**
 *
 * @author frank@harenberg.ch
 */
@Stateless
@LocalBean
public class updateSiteReport {

    /**
     * timer, will trigger everyday at 20:08 the retrieval of the site report
     */
    @Schedule(minute = "8", second = "0", dayOfMonth = "*", month = "*", year = "*", hour = "22", dayOfWeek = "*")
    public void siteReportTimer() {
        Logger.getLogger(getNewLicences.class.getName()).log(Level.INFO, "Periodic update of site summary report");
        tenantController tc = new tenantController();
        tc.updateSiteReport();
    }
}
