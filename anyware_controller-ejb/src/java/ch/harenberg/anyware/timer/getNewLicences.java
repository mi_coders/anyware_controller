/*
 * Copyright (C) 2014 SSD-TCHHAFR1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ch.harenberg.anyware.timer;

import ch.harenberg.anyware.boundary.tenantController;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;


/**
 *
 * @author SSD-TCHHAFR1
 */
@Stateless
@LocalBean
public class getNewLicences {

    @Schedule(minute = "5,10,15,20,25,30,35,40,45,55,59", second = "0", dayOfMonth = "*", month = "*", year = "*", hour = "*", dayOfWeek = "*")
    public void checkForNewLicences() {
        Logger.getLogger(getNewLicences.class.getName()).log(Level.INFO, "Periodic Check for pending orders");
        tenantController tc = new ch.harenberg.anyware.boundary.tenantController();
        tc.updateTenantLicences();
    }
}
