/*
 * Copyright (C) 2014 SSD-TCHHAFR1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ch.harenberg.anyware.session;

import ch.harenberg.anyware.entity.Tenant;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


/**
 *
 * @author SSD-TCHHAFR1
 */
@Stateless
public class TenantFacade extends AbstractFacade<Tenant> {
    @PersistenceContext(unitName = "anyware_controller-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TenantFacade() {
        super(Tenant.class);
    }

    /**
     * @since 1.0
     * @return Collection of tenants with pending license orders
     */
    public Collection initializeTenants() {
        Query query = em.createNamedQuery("Tenant.findByLicenceKey");
        query.setParameter("licenceKey", "Pending");
        //Retrieve all tenants with a pending licence
        Collection tenants = query.getResultList();
        return tenants;
    }
    
    /**
     * Will lookup a tenant-object in the database by TenantId
     * @since 1.0
     * @param tenantId
     * @return Tenant matching tenantId
     * 
     */
    public Tenant findByTenantId(int tenantId){
        Query query = em.createNamedQuery("Tenant.findByTenantId");
        query.setParameter("tenantId", tenantId);
        try{
            return (Tenant) query.getSingleResult();
        }catch(javax.persistence.NoResultException ex){
            return null;
        }
        
    }
    
    
    
}
