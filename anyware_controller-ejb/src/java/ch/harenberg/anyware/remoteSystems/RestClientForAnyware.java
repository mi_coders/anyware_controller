/*
 * Copyright (C) 2013 Frank Harenberg, frank@harenberg.ch
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.harenberg.anyware.remoteSystems;

import ch.harenberg.anyware.entity.Tenant;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PutMethod;
import org.netbeans.saas.RestConnection;
import org.netbeans.saas.RestResponse;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Class constructor, will read in the connection properties from property file
 * in same package
 *
 * @author Frank Harenberg, frank@harenberg.ch
 *
 */
public class RestClientForAnyware {

    private static String anywareHost;
    private static String anywareAdmin;
    private static String anywarePass;

    private static final Logger LOG = Logger.getLogger(RestClientForAnyware.class.getName());
    private static final String PROP_FILE = RestClientForAnyware.class.getSimpleName().toLowerCase() + ".properties";

    static {

        try {
            Properties props = new Properties();
            props.load(RestClientForAnyware.class.getResourceAsStream(PROP_FILE));
            anywareHost = props.getProperty("anywareHost");
            anywareAdmin = props.getProperty("anywareAdmin");
            anywarePass = props.getProperty("anywarePass");
        } catch (IOException ex) {
            Logger.getLogger(RestClientForAnyware.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * getAnywareHost: checks if "anywareHost" property is set in class property
     * file and will return it
     *
     * @since 1.0
     * @return https://+anywareHost property as string
     * @throws IOException
     */
    private String getAnywareHost() throws IOException {
        if (anywareHost == null || anywareHost.length() == 0) {
            throw new IOException("Please specify the anyware hostname in the " + PROP_FILE + " file.");
        }
        return "https://" + anywareHost;
    }

    /**
     * getAnywareAdmin: checks if "anywareAdmin" property is set in class
     * property file and will return it
     *
     * @since 1.0
     * @return anywareAdmin property as string
     * @throws IOException
     */
    private String getAnywareAdmin() throws IOException {
        if (anywareAdmin == null || anywareAdmin.length() == 0) {
            throw new IOException("Please specify the anyware Admin login in the " + PROP_FILE + " file.");
        }
        return anywareAdmin;
    }

    /**
     * getAnywarePass: checks if "anywarePass" property is set in class property
     * file and will return it
     *
     * @since 1.0
     * @return anywarePass property as string
     * @throws IOException
     */
    private String getAnywarePass() throws IOException {
        if (anywarePass == null || anywarePass.length() == 0) {
            throw new IOException("Please specify the anyware Admin password in the " + PROP_FILE + " file.");
        }
        return anywarePass;
    }

    /**
     * <strong>createTenant</strong> Send the request for creating new tenant
     * <p>
     * Call to anyware platform description:<br>
     * <strong>URL:</strong><code> $anywareHost$/api/v1/tenant</code><br>
     * <strong>Method: </strong>POST<br>
     * <strong>Input Parameters</strong><br><ul>
     * <ul>
     * <li><strong>tenantName: </strong><code>$CustomerName$</code></li>
     * <li><strong>organizationName: </strong><code>$CustomerName$</code></li>
     * <li><strong>accountSource: </strong><code>"LOCAL"</code></li>
     * <li><strong>displayName: </strong><code>$CustomerName$</code></li>
     * <li><strong>firstName: </strong><code>$AdminFirstName$</code></li>
     * <li><strong>lastName: </strong><code>$AdminLastName$</code></li>
     * <li><strong>uid: </strong><code>$SCSAdminMail$</code> setting an internal
     * swisscom user as tenant admin</li>
     * <li><strong>emailAddress: </strong><code>$SCSAdminMail$</code></li>
     * <li><strong>password: </strong><code>$SCSAdminPassword$</code></li>
     * <li><strong>domains: </strong><code>$CustomerDomain$</code></li>
     * <li><strong>whiteLabelKey: </strong><code>"swisscom"</code></li>
     * <li><strong>sendActivation: </strong><code>"false"</code></li>
     * </ul>
     * <br>
     * <strong>Request Headers</strong><br><ul>
     * <li><strong>Authorization:
     * </strong><code>"Basic + base64encoded Platfrom-Admin Credentials"</code></li>
     * <li><strong>Accept-Language: </strong><code>$AdminLang$</code> Will set
     * default tenant language</li>
     * </ul>
     *
     * @param myTenant
     * @return tenant after creation with updated tenantId, CmPartitionId and
     * DmPartitionId
     * @throws IOException
     * @throws ParseException
     */
    public Tenant createTenant(Tenant myTenant) throws IOException, ParseException {
        String[][] headers = new String[][]{
            {"Authorization", this.authString()},
            {"Accept-Language", myTenant.getAdminLang()},};

        String whiteLabelKey = "swisscom";

        String[][] params = new String[][]{
            {"tenantName", myTenant.getCustomerName()},
            {"organizationName", myTenant.getCustomerName()},
            {"accountSource", "LOCAL"},
            {"displayName", myTenant.getCustomerName()},
            {"firstName", myTenant.getAdminFirstName()},
            {"lastName", myTenant.getAdminLastName()},
            {"uid", myTenant.getSCSAdminMail()},
            {"emailAddress", myTenant.getSCSAdminMail()},
            {"password", myTenant.getSCSAdminPassword()},
            {"domains", myTenant.getCustomerDomain()},
            {"whiteLabelKey", whiteLabelKey},
            {"sendActivation", "false"}

        };
        RestConnection rc1 = new RestConnection(this.getAnywareHost() + "/api/v1/tenant");
        LOG.log(Level.INFO, "Submitting createTenant {0}", this.encodeParams(params));
        RestResponse res = rc1.post(headers, params);
        String sRet = res.getDataAsString();
        LOG.log(Level.INFO, "Got response from upstream server for tenant {0}: {1}", new Object[]{(String) myTenant.getCustomerName(), sRet});
        JSONParser parser = new JSONParser();
        JSONObject ojson = (JSONObject) parser.parse(sRet);
        JSONObject result = (JSONObject) ojson.get("result");
        JSONObject tenant = (JSONObject) result.get("tenant");
        Integer tenantId = (int) (long) tenant.get("id");
        myTenant.setTenantId(tenantId);
        myTenant.setTenantDmPartitionId((int) (long) tenant.get("defaultDmPartitionId"));
        myTenant.setTenantCmPartitionId((int) (long) tenant.get("defaultCmPartitionId"));
        //}

        return myTenant;
    }

    /**
     * <strong>acceptEulaForTenant</strong> Sets the EULA as accepted
     * <p>
     * Call to anyware platform description:<br>
     * <strong>URL:</strong><code> $anywareHost$/api/v1/tenant/eula</code><br>
     * <strong>Method: </strong>POST<br>
     * <strong>Input Parameters</strong><br><ul>
     * <ul>
     * <li><strong>firstName: </strong><code>$AdminFirstName$</code></li>
     * <li><strong>lastName: </strong><code>$AdminLastName$</code></li>
     * <li><strong>eulaId: </strong><code>"46009"</code></li>
     * <li><strong>company: </strong><code>$CustomerName$</code></li>
     * <li><strong>address: </strong>$CustomerStreet$<code></code></li>
     * <li><strong>city: </strong><code>$CustomerCity$</code></li>
     * <li><strong>stateOrProvince: </strong><code>$CustomerState$</code></li>
     * <li><strong>zipCode: </strong><code>$CustomerZIP$</code></li>
     * <li><strong>country: </strong><code>CustomerCountry</code></li>
     * <li><strong>productName:
     * </strong><code>"Welcome to Swisscom MDS"</code></li>
     * <li><strong>email: </strong><code>$CustomerMail$</code></li>
     * </ul>
     * <br>
     * <strong>Request Headers</strong><br><ul>
     * <li><strong>Authorization:
     * </strong><code>"Basic + base64encoded SCSTenantAdminUser"</code></li>
     * </ul>
     *
     * @param myTenant
     */
    public void acceptEulaForTenant(Tenant myTenant) {
        try {
            String[][] headers = new String[][]{{"Authorization", this.tenantAuthString(myTenant.getSCSAdminMail(), myTenant.getSCSAdminPassword())},};
            String[][] args = new String[][]{
                {"firstName", myTenant.getAdminFirstName()},
                {"lastName", myTenant.getAdminLastName()},
                {"eulaId", "46009"},
                {"company", myTenant.getCustomerName()},
                {"address", myTenant.getCustomerStreet()},
                {"city", myTenant.getCustomerCity()},
                {"stateOrProvince", myTenant.getCustomerState()},
                {"zipCode", myTenant.getCustomerZIP()},
                {"country", myTenant.getCustomerCountry()},
                {"productName", "Welcome to Swisscom MDS"},
                {"email", myTenant.getCustomerMail()},
                {"text", "SCS_EULA"}
            };
            LOG.log(Level.INFO, "Running acceptEulaForTenant for tenant {0}", this.encodeParams(args));
            RestConnection rc1 = new RestConnection(this.getAnywareHost() + "/api/v1/tenant/eula");
            RestResponse res = rc1.post(headers, args);
            String sRet = res.getDataAsString();
            LOG.log(Level.INFO, "Got response from upstream", sRet);
        } catch (IOException ex) {
            Logger.getLogger(RestClientForAnyware.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * <strong>createTenantSuperUser</strong> creates a user intended for tenant
     * super user
     * <p>
     * Call to anyware platform description:<br>
     * <strong>URL:</strong><code> $anywareHost$/api/v1/account</code><br>
     * <strong>Method: </strong>POST<br>
     * <strong>Input Parameters</strong><br><ul>
     * <ul>
     * <li><strong>sendInvite: </strong><code>"false"</code></li>
     * <li><strong>accountSource: </strong><code>"LOCAL"</code></li>
     * <li><strong>firstName: </strong><code>$AdminFirstName$</code></li>
     * <li><strong>lastName: </strong><code>$AdminLastName$</code></li>
     * <li><strong>uid: </strong><code>$CustomerMail$</code></li>
     * <li><strong>emailAddress: </strong><code>$CustomerMail$</code></li>
     * <li><strong>displayName:
     * </strong><code>"Admin "AdminFirstName AdminLastName</code></li>
     * <li><strong>password: </strong><code>$AdminPassword$</code></li>
     * <li><strong>confirmPassword: </strong><code>$AdminPassword$</code></li>
     * </ul>
     * <br>
     * <strong>Request Headers</strong><br><ul>
     * <li><strong>Authorization:
     * </strong><code>"Basic + base64encoded SCSTenantAdminUser"</code></li>
     * </ul>
     *
     * @param myTenant
     * @return UserId of TenantSuperUser
     */
    public Integer createTenantSuperUser(Tenant myTenant) {
        try {
            String[][] headers = new String[][]{
                {"Authorization", this.tenantAuthString(myTenant.getSCSAdminMail(), myTenant.getSCSAdminPassword())},};
            String[][] args = new String[][]{
                {"sendInvite", "false"},
                {"accountSource", "LOCAL"},
                {"uid", myTenant.getCustomerMail()},
                {"emailAddress", myTenant.getCustomerMail()},
                {"firstName", myTenant.getAdminFirstName()},
                {"lastName", myTenant.getAdminLastName()},
                {"displayName", "Admin" + myTenant.getAdminFirstName() + " " + myTenant.getAdminLastName()},
                {"password", myTenant.getAdminPassword()},
                {"confirmPassword", myTenant.getAdminPassword()}
            };
            LOG.log(Level.INFO, "Running createTenantSuperUser for tenant {0}", this.encodeParams(args));
            RestConnection rc1 = new RestConnection(this.getAnywareHost() + "/api/v1/account");
            RestResponse res = rc1.post(headers, args);
            String sRet = res.getDataAsString();
            LOG.log(Level.INFO, "Got response from upstream server for tenant {0}: {1}", new Object[]{(String) myTenant.getCustomerName(), sRet});
            JSONParser parser = new JSONParser();
            JSONObject ojson = (JSONObject) parser.parse(sRet);
            JSONObject result = (JSONObject) ojson.get("result");
            JSONArray Accounts = (JSONArray) result.get("validAccounts");
            JSONObject tenantAccount = (JSONObject) Accounts.get(0);
            int tenantAccountId = (int) (long) tenantAccount.get("id");

            return tenantAccountId;
        } catch (IOException ex) {
            Logger.getLogger(RestClientForAnyware.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(RestClientForAnyware.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    /**
     * encodeParams: will urlencode a String[][] to a string to be submittet in
     * other rest calls
     *
     * @param params
     * @return
     */
    private String encodeParams(String[][] params) {
        String p = "";

        if (params != null) {
            for (int i = 0; i < params.length; i++) {
                String key = params[i][0];
                String value = params[i][1];

                if (value != null) {
                    try {
                        p += key + "=" + URLEncoder.encode(value, "UTF-8") + "&";
                    } catch (UnsupportedEncodingException ex) {
                        Logger.getLogger(RestConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            if (p.length() > 0) {
                p = p.substring(0, p.length() - 1);
            }
        }

        return p;
    }

    /**
     * <strong>getFirstTenantRole</strong> searches for available tenant roles
     * and returns the first one
     * <p>
     * Call to anyware platform description:<br>
     * <strong>URL:</strong><code> $anywareHost$/api/v1/role</code><br>
     * <strong>Method: </strong>GET<br>
     * <br>
     * <strong>Request Headers</strong><br><ul>
     * <li><strong>Authorization:
     * </strong><code>"Basic + base64encoded SCSTenantAdminUser"</code></li>
     * </ul>
     *
     * @param myTenant
     * @return firstRoleId
     */
    public int getFirstTenantRole(Tenant myTenant) {
        try {
            String[][] headers = new String[][]{
                {"Authorization", this.tenantAuthString(myTenant.getSCSAdminMail(), myTenant.getSCSAdminPassword())},};
            LOG.log(Level.INFO, "Running getTenantRoleList for tenant {0}", (String) myTenant.getCustomerName());
            RestConnection rc1 = new RestConnection(this.getAnywareHost() + "/api/v1/role");
            RestResponse res = rc1.get(headers);
            String sRet = res.getDataAsString();
            LOG.log(Level.INFO, "Got response from upstream server for tenant {0}: {1}", new Object[]{(String) myTenant.getCustomerName(), sRet});
            JSONParser parser = new JSONParser();
            JSONObject ojson = (JSONObject) parser.parse(sRet);
            JSONObject result = (JSONObject) ojson.get("result");
            JSONArray aryresult = (JSONArray) result.get("searchResults");
            JSONObject firstRole = (JSONObject) aryresult.get(0);
            int firstRoleId = (int) (long) firstRole.get("id");
            return firstRoleId;
        } catch (IOException ex) {
            Logger.getLogger(RestClientForAnyware.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        } catch (ParseException ex) {
            Logger.getLogger(RestClientForAnyware.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    /**
     * <strong>assignCustomerAdminRoles</strong> assigns all roles to
     * customer-Admin
     * <p>
     * Call to anyware platform description:<br>
     * <strong>URL:</strong><code> $anywareHost$/api/v1/account/$customerAdminId$/roles</code><br>
     * <strong>Method: </strong>PUT<br>
     * <strong>Input Parameters</strong><br><ul>
     * <ul>
     * <li><strong>append</strong><code>"false"</code></li>
     * <li><strong>roles[0].roleId</strong><code>tenantFirstRoleId)</code></li>
     * <li><strong>roles[1].roleId</strong><code>tenantFirstRoleId + 1</code></li>
     * <li><strong>roles[2].roleId</strong><code>tenantFirstRoleId + 2</code></li>
     * <li><strong>roles[3].roleId</strong><code>tenantFirstRoleId + 3</code></li>
     * <li><strong>roles[4].roleId</strong><code>tenantFirstRoleId + 4</code></li>
     * <li><strong>roles[4].dmPartitionId</strong><code>TenantDmPartitionId</code></li>
     * <li><strong>roles[5].roleId</strong><code>tenantFirstRoleId + 5</code></li>
     * <li><strong>roles[5].dmPartitionId</strong><code>TenantDmPartitionId</code></li>
     * <li><strong>roles[6].roleId</strong><code>tenantFirstRoleId + 6</code></li>
     * <li><strong>roles[6].cmPartitionId</strong><code>TenantCmPartitionId</code></li>
     * <li><strong>roles[7].roleId</strong><code>tenantFirstRoleId + 7</code></li>
     * <li><strong>roles[7].cmPartitionId</strong><code>TenantCmPartitionId</code></li>
     * <li><strong>roles[8].roleId</strong><code>tenantFirstRoleId + 8</code></li>
     * <li><strong>roles[9].roleId</strong><code>tenantFirstRoleId + 9</code></li>
     * <li><strong>roles[9].cmPartitionId</strong><code>TenantCmPartitionId</code></li>
     * </ul>
     * <br>
     * <strong>Request Headers</strong><br><ul>
     * <li><strong>Authorization:
     * </strong><code>"Basic + base64encoded SCSTenantAdminUser"</code></li>
     * </ul>
     *
     * @param myTenant
     * @param customerAdminId
     * @param tenantFirstRoleId
     */
    public void assignCustomerAdminRoles(Tenant myTenant, int customerAdminId, int tenantFirstRoleId) throws IOException {
        try {
            String[][] headers = new String[][]{
                {"Authorization", this.tenantAuthString(myTenant.getSCSAdminMail(), myTenant.getSCSAdminPassword())},};

            String[][] args = new String[][]{
                {"append", "false"},
                {"roles[0].roleId", String.valueOf(tenantFirstRoleId)},
                {"roles[1].roleId", String.valueOf(tenantFirstRoleId + 1)},
                {"roles[2].roleId", String.valueOf(tenantFirstRoleId + 2)},
                {"roles[3].roleId", String.valueOf(tenantFirstRoleId + 3)},
                {"roles[4].roleId", String.valueOf(tenantFirstRoleId + 4)},
                {"roles[4].dmPartitionId", String.valueOf(myTenant.getTenantDmPartitionId())},
                {"roles[5].roleId", String.valueOf(tenantFirstRoleId + 5)},
                {"roles[5].dmPartitionId", String.valueOf(myTenant.getTenantDmPartitionId())},
                {"roles[6].roleId", String.valueOf(tenantFirstRoleId + 6)},
                {"roles[6].cmPartitionId", String.valueOf(myTenant.getTenantCmPartitionId())},
                {"roles[7].roleId", String.valueOf(tenantFirstRoleId + 7)},
                {"roles[7].cmPartitionId", String.valueOf(myTenant.getTenantCmPartitionId())},
                {"roles[8].roleId", String.valueOf(tenantFirstRoleId + 8)},
                {"roles[9].roleId", String.valueOf(tenantFirstRoleId + 9)},
                {"roles[9].cmPartitionId", String.valueOf(myTenant.getTenantCmPartitionId())}
            };
            LOG.log(Level.INFO, "Running assignCustomerAdminRoles for tenant {0}", this.encodeParams(args));
            String arg = this.encodeParams(args);
            HttpClient httpclient = new HttpClient();
            String serviceurl = this.getAnywareHost() + "/api/v1/account/" + customerAdminId + "/roles";
            PutMethod pm = new PutMethod(serviceurl);

            // set the authentication in the header
            pm.setRequestHeader("Authorization", this.tenantAuthString(myTenant.getSCSAdminMail(), myTenant.getSCSAdminPassword()));
            pm.setQueryString(arg);

            httpclient.executeMethod(pm);
            String sRet = pm.getResponseBodyAsString();

            LOG.log(Level.INFO, "Got response from upstream server for tenant {0}: {1}", new Object[]{(String) myTenant.getCustomerName(), sRet});
        } catch (IOException ex) {
            Logger.getLogger(RestClientForAnyware.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    /**
     * /
     **
     * <strong>applyLicence</strong> Applies license-key to tenant
     * <p>
     * Call to anyware platform description:<br>
     * <strong>URL:</strong><code> $anywareHost$/api/v1/tenant/$tenantId$/license/activate</code><br>
     * <strong>Method: </strong>POST<br>
     * <strong>Input Parameters</strong><br><ul>
     * <ul>
     * <li><strong>licenseKey: </strong><code>$LicenceKey$</code></li>
     * </ul>
     * <br>
     * <strong>Request Headers</strong><br><ul>
     * <li><strong>Authorization:
     * </strong><code>"Basic + base64encoded Platfrom-Admin Credentials"</code></li>
     * </ul>
     *
     * @param myTenant
     */
    public void applyLicence(Tenant myTenant) {
        try {
            String[][] headers = new String[][]{{"Authorization", this.authString()},};
            String[][] args = new String[][]{{"licenseKey", myTenant.getLicenceKey()},};
            LOG.log(Level.INFO, "Running applyLicence for tenant {0}", (String) myTenant.getCustomerName());
            RestConnection rc1 = new RestConnection(this.getAnywareHost() + "/api/v1/tenant/" + myTenant.getTenantId() + "/license/activate");
            RestResponse res = rc1.post(headers, args);
            String sRet = res.getDataAsString();
            LOG.log(Level.INFO, "Got response: {0}", sRet);
        } catch (IOException ex) {
            Logger.getLogger(RestClientForAnyware.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns the base64-encoded String of Platform admin for http basic
     * authentication
     *
     * @return "Basic base64code"
     */
    private String authString() {
        String authString = null;
        try {
            authString = this.getAnywareAdmin() + ":" + this.getAnywarePass();
            authString = new String((Base64.encodeBase64(authString.getBytes())));

        } catch (IOException ex) {
            Logger.getLogger(RestClientForAnyware.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Basic " + authString;
    }

    /**
     * Returns the base64-encoded String of SCSTenantSuperUser for http basic
     * authentication
     *
     * @param tenantUser
     * @param tenantPass
     * @return "Basic base64code"
     */
    private String tenantAuthString(String tenantUser, String tenantPass) {
        String authString = null;

        authString = tenantUser + ":" + tenantPass;
        authString = new String((Base64.encodeBase64(authString.getBytes())));

        return "Basic " + authString;
    }

    /**
     * <strong>getPlatformReport</strong> Retrieves the sitesummary report as
     * JSONArray
     * <br>In a first call, will call the sitesummary and retrieve the number of
     * items, then recall the sitesummary with option rows=number of items
     * <p>
     * Call to anyware platform description:<br>
     * <strong>URL:</strong><code> $anywareHost$/api/v1/report/sitesummary</code><br>
     * <strong>Method: </strong>GET<br>
     * <br>
     * <strong>Request Headers</strong><br><ul>
     * <li><strong>Authorization:
     * </strong><code>"Basic + base64encoded Platfrom-Admin Credentials"</code></li>
     * </ul>
     *
     * @return PlatformReport as JSONArray
     */
    public JSONArray getPlatformReport() {
        try {
            String[][] headers = new String[][]{{"Authorization", this.authString()},};
            LOG.log(Level.INFO, "Running getPlatformReport");
            RestConnection rc1 = new RestConnection(this.getAnywareHost() + "/api/v1/report/sitesummary");
            RestResponse res = rc1.get(headers);
            String sRet = res.getDataAsString();
            JSONParser parser = new JSONParser();
            JSONObject ojson = (JSONObject) parser.parse(sRet);
            JSONObject result = (JSONObject) ojson.get("result");
            JSONObject summary = (JSONObject) result.get("summary");
            int count = (int) (long) summary.get("totalCount");
            Logger.getLogger(RestClientForAnyware.class.getName()).log(Level.INFO,"report contains {0} rows", String.valueOf(count));
            RestConnection rc2 = new RestConnection(this.getAnywareHost() + "/api/v1/report/sitesummary?rows=" + String.valueOf(count));
            res = rc2.get(headers);
            ojson = (JSONObject) parser.parse(res.getDataAsString());
            result = (JSONObject) ojson.get("result");
            summary = (JSONObject) result.get("summary");
            Logger.getLogger(RestClientForAnyware.class.getName()).log(Level.INFO, res.getDataAsString());
            return (JSONArray) summary.get("searchResults");
        } catch (IOException | ParseException ex) {
            Logger.getLogger(RestClientForAnyware.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
