/*
 * Copyright (C) 2013 Frank Harenberg, frank@harenberg.ch
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.harenberg.anyware.remoteSystems;

import ch.harenberg.anyware.entity.Tenant;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.saas.RestConnection;
import org.netbeans.saas.RestResponse;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author fhadmin
 *
 * This class represents the REST Methods to Salesforce for Polaris Licensing
 */
public class RestClientForSFDC {

    // Variables to be populated after gaining the Access token.
    private String accessToken = null;
    private String instanceUrl;

    // Constants to be used in this class.
    private static final String authUrl = "/services/oauth2/token";
    private static final String operatorOrderUrl = "/services/data/v29.0/sobjects/OperatorOrder__c/";

    private static String securityToken;
    private static String username;
    private static String password;
    private static String environment;

    private static String clientId;
    private static String clientSecret;
    private static final Logger LOG = Logger.getLogger(RestClientForSFDC.class.getName());

    private static final String PROP_FILE = RestClientForSFDC.class.getSimpleName().toLowerCase() + ".properties";

    /**
     * Constructor for the class, will read in the service specific property
     * file and retrieve the salesforce oauth2 tokens by calling class method
     * oAuthSessionProvider
     *
     * @throws IOException
     * @throws ParseException
     */
    public RestClientForSFDC() throws IOException, ParseException {
        try {
            Properties props = new Properties();
            props.load(RestClientForSFDC.class.getResourceAsStream(PROP_FILE));
            environment = props.getProperty("environment");
            username = props.getProperty("username");
            password = props.getProperty("password");
            clientId = props.getProperty("clientId");
            clientSecret = props.getProperty("clientSecret");
            securityToken = props.getProperty("SecurityToken");
        } catch (IOException ex) {
            Logger.getLogger(RestClientForAnyware.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.oAuthSessionProvider();
    }

    /**
     * getEnvironment: checks if "environment" property is set in class property
     * file and will return it
     *
     * @since 1.0
     * @return https:// + environment property as string
     * @throws IOException
     */
    private static String getEnvironment() throws IOException {
        if (environment == null || environment.length() == 0) {
            throw new IOException("Please specify the salesforce environment in the " + PROP_FILE + " file.");
        }
        return "https://" + environment;
    }

    /**
     * getUsername: checks if "username" property is set in class property file
     * and will return it
     *
     * @since 1.0
     * @return username property as string
     * @throws IOException
     */
    private static String getUsername() throws IOException {
        if (username == null || username.length() == 0) {
            throw new IOException("Please specify the salesforce username in the " + PROP_FILE + " file.");
        }
        return username;
    }

    /**
     * getPassword: checks if "password" property is set in class property file
     * and will return it
     *
     * @since 1.0
     * @return password property as string
     * @throws IOException
     */
    private static String getPassword() throws IOException {
        if (password == null || password.length() == 0) {
            throw new IOException("Please specify the salesforce password in the " + PROP_FILE + " file.");
        }
        return password;
    }

    /**
     * getClientId: checks if "clientId" property is set in class property file
     * and will return it
     *
     * @since 1.0
     * @return clientId property as string
     * @throws IOException
     */
    private static String getClientId() throws IOException {
        if (clientId == null || clientId.length() == 0) {
            throw new IOException("Please specify the salesforce clientId in the " + PROP_FILE + " file.");
        }
        return clientId;
    }

    /**
     * getClientSecret: checks if "clientSecret" property is set in class
     * property file and will return it
     *
     * @since 1.0
     * @return clientSecret property as string
     * @throws IOException
     */
    private static String getClientSecret() throws IOException {
        if (clientSecret == null || clientSecret.length() == 0) {
            throw new IOException("Please specify the salesforce clientSecret in the " + PROP_FILE + " file.");
        }
        return clientSecret;
    }

    /**
     * getSecurityToken: checks if "securityToken" property is set in class
     * property file and will return it
     *
     * @since 1.0
     * @return securityToken property as string
     * @throws IOException
     */
    private static String getSecurityToken() throws IOException {
        if (securityToken == null || securityToken.length() == 0) {
            throw new IOException("Please specify the salesforce clientSecret in the " + PROP_FILE + " file.");
        }
        return securityToken;
    }

    /**
     * oAuthSessionProvider: Connects to Salesforce and retrieves an oAuth Token
     * and Hostname for subsequent calls. This is needed for all subsequent
     * calls to Salesforce for authentication.
     * <br>
     * hostname is either test.salesforce.com or login.salesforce.com
     * <p>
     * Call to Salesforce description:<br>
     * <strong>URL:</strong><code> $hostname$/services/oauth2/token</code><br>
     * <strong>Method: </strong>POST<br>
     * <strong>Input Parameters</strong><br><ul>
     * <li><strong>grant_type: </strong><code>"password"</code></li>
     * <li><strong>username: </strong><code>$salesforce_login$</code></li>
     * <li><strong>password: </strong><code>$salesforce_password$</code> In
     * preprod, the additional SecurityToken is appended</li>
     * <li><strong>client_id: </strong><code>$client_id$</code> provided by
     * MobileIron</li>
     * <li><strong>client_secret: </strong><code>$client_secret$</code> provided
     * by MobileIron</li>
     * </ul>
     * <br>
     * <strong>Request Headers</strong><br><ul>
     * <li><strong>Accept: </strong><code>"application/json"</code></li>
     * </ul>
     *
     *
     * @throws IOException
     * @throws ParseException
     */
    private void oAuthSessionProvider() throws IOException, ParseException {

        String baseUrl = this.getEnvironment() + authUrl;
        String securityToken = null;
        if (!this.getSecurityToken().equals("prod")) {
            securityToken = this.getSecurityToken();
        }

        // The request body must contain these 5 values.
        String[][] params = new String[][]{
            {"grant_type", "password"},
            {"username", this.getUsername()},
            {"password", this.getPassword() + securityToken},
            {"client_id", this.getClientId()},
            {"client_secret", this.getClientSecret()}
        };

        //we have no additional request headers, but make sure to get json in return
        String[][] headers = new String[][]{
            {"Accept", "application/json"}
        };

        LOG.log(Level.INFO, "POST{0}", baseUrl);
        RestConnection rc1 = new RestConnection(baseUrl);
        RestResponse res = rc1.post(headers, params);
        String sRet = res.getDataAsString();
        LOG.log(Level.INFO, "OAuth login response: {0}", sRet);

        // populate Response map to fetch ACCESS TOKEN for any future call.
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(sRet);

        // Populate Access Token
        this.accessToken = json.get("access_token").toString();
        this.instanceUrl = json.get("instance_url").toString();
    }

    /**
     * Send the request for the New License
     * <p>
     * Call to Salesforce description:<br>
     * <strong>URL:</strong><code> $oAuthinstanceURL$/services/data/v29.0/sobjects/OperatorOrder__c/</code><br>
     * <strong>Method: </strong>POST<br>
     * <strong>Input Parameters</strong><br><ul>
     * <li><strong>OperatorCustomerId__c: </strong><code>$BSK$</code></li>
     * <li><strong>Customer_Name__c: </strong><code>$CustomerName$</code></li>
     * <li><strong>Customer_Street__c: </strong><code>$CustomerStreet$</code></li>
     * <li><strong>Customer_City__c: </strong><code>$CustomerCity$</code></li>
     * <li><strong>Customer_State__c: </strong><code>$CustomerState$</code></li>
     * <li><strong>Customer_Country__c: </strong><code>$CustomerCountry$</code></li>
     * <li><strong>Customer_Zip_Code__c: </strong><code>$CustomerZIP$</code></li>
     * <li><strong>Admin_First_Name__c: </strong><code>$AdminFirstName$</code></li>
     * <li><strong>Admin_Last_Name__c: </strong><code>$AdminLastName$</code></li>
     * <li><strong>Admin_Email_ID__c: </strong><code>$SCSAdminMail$</code> We send a bogus mail, the one to be used as Swisscom controlled tenant admin</li>
     * <li><strong>Domain__c: </strong><code>$CustomerDomain$</code></li>
     * <li><strong>Admin_Password__c: </strong><code>"myPa$$W0rd"</code> bogus password</li>
     * <li><strong>License_Order_Type__c: </strong><code>"Get new License"</code> static</li>
     * <li><strong>BillingType__c: </strong><code>"Usage Based"</code></li>
     * <li><strong>MI_ANYWARE_FOUNDATION__c: </strong><code>"true"</code> always for prod license</li>
     * <li><strong>MI_ANYWARE_Apps__c: </strong><code>true/false</code> if applicable in prod license</li>
     * <li><strong>MI_ANYWARE_Content__c: </strong><code>true/false</code> if applicable in prod license</li>
     * <li><strong>MI_ANYWARE_Security__c: </strong><code>true/false</code> if applicable in prod license</li>
     * <li><strong>EVAL_MI_ANYWARE_FOUNDATION__c: </strong><code>"true"</code> always for trial license</li>
     * </ul>
     * <br>
     * <strong>Request Headers</strong><br><ul>
     * <li><strong>Accept: </strong><code>"application/json"</code></li>
     * <li><strong>Authorization: </strong><code> "Bearer " + $accessToken$</code></li>
     * </ul>
     *
     * @param myTenant
     * @return String OrderId/LicenseId
     * @throws Exception
     * 
     */
    public String createOperatorOrderForNewLicense(Tenant myTenant) throws Exception {

        try {

            String baseUrl = this.instanceUrl + operatorOrderUrl;

            //Setting request headers with authentication token
            String[][] headers = new String[][]{
                {"Authorization", "Bearer " + this.accessToken},
                {"Content-Type", "application/json"}
            };

            LOG.log(Level.INFO, "POST {0}", baseUrl);
            RestConnection rc1 = new RestConnection(baseUrl);

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("OperatorCustomerId__c", myTenant.getBsk());

            // Customer Account Information
            jsonObject.put("Customer_Name__c", myTenant.getCustomerName());
            jsonObject.put("Customer_Street__c", myTenant.getCustomerStreet());
            jsonObject.put("Customer_City__c", myTenant.getCustomerCity());
            jsonObject.put("Customer_State__c", myTenant.getCustomerState());
            jsonObject.put("Customer_Country__c", myTenant.getCustomerCountry());
            jsonObject.put("Customer_Zip_Code__c", myTenant.getCustomerZIP());

            // Tenant Admin for Customer
            jsonObject.put("Admin_First_Name__c", myTenant.getAdminFirstName());
            jsonObject.put("Admin_Last_Name__c", myTenant.getAdminLastName());
            jsonObject.put("Admin_Email_ID__c", myTenant.getSCSAdminMail());
            jsonObject.put("Domain__c", myTenant.getCustomerDomain());
            jsonObject.put("Admin_Password__c", "myPa$$W0rd");

            // License Info
            if (myTenant.getIsTrial() == false) {
                // OrderType Information for paying licence
                jsonObject.put("License_Order_Type__c", "Get new License");
                jsonObject.put("BillingType__c", "Usage Based");
                jsonObject.put("MI_ANYWARE_FOUNDATION__c", true);
                jsonObject.put("MI_ANYWARE_Apps__c", myTenant.getMianywareapps());
                jsonObject.put("MI_ANYWARE_Content__c", myTenant.getMianywarecontent());
                jsonObject.put("MI_ANYWARE_Security__c", myTenant.getMianywaresecurity());
            } else {
                // OrderType Information for trial license
                jsonObject.put("License_Order_Type__c", "Get new License");
                jsonObject.put("BillingType__c", "Usage Based");
                jsonObject.put("EVAL_MI_ANYWARE_FOUNDATION__c", true);
            }

            RestResponse res = rc1.post(headers, jsonObject.toJSONString());
            LOG.log(Level.INFO, "requesting licence:{0}", jsonObject.toJSONString());

            String responseBody = res.getDataAsString();
            LOG.log(Level.INFO, "got response: {0}", responseBody);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(responseBody);

            if ((Boolean) json.get("success")) {
                String sret = (String) json.get("id");
                LOG.log(Level.INFO, "obtained id: {0}", sret);
                return sret;
            }

        } catch (Exception e) {
            LOG.log(Level.SEVERE, "error ordering licence", e.getMessage());
        }
        return null;

    }

    /**
     * Get Status and license key by Id from the Salesforce Instance
     *
     * <p>
     * Call to Salesforce description:<br>
     * <strong>URL:</strong><code> $oAuthinstanceURL$/services/data/v29.0/sobjects/OperatorOrder__c/$LicenseId$</code><br>
     * <strong>Method: </strong>POST<br>
     * <ul>
     * <li><strong>fields: </strong><code>"ID,Name,License_Key__c,OrderStatus__c"</code></li>
     * </ul>
     * <br>
     * <strong>Request Headers</strong><br><ul>
     * <li><strong>Accept: </strong><code>"application/json"</code></li>
     * <li><strong>Authorization: </strong><code> "Bearer " + $accessToken$</code></li>
     * </ul>
     * 
     * @param myTenant
     * @return String containing either license Key or "Pending"
     * @throws Exception
     */
    public String getOperatorOrderById(Tenant myTenant) throws Exception {

        try {
            String objectId = myTenant.getLicenceID();
            LOG.log(Level.INFO, "Getting Information for order {0}", objectId);

            String baseUrl = this.instanceUrl + operatorOrderUrl + objectId + "?fields=ID,Name,License_Key__c,OrderStatus__c";

            //Setting request headers with authentication token
            String[][] headers = new String[][]{
                {"Authorization", "Bearer " + this.accessToken},
                {"Content-Type", "application/json"}
            };

            LOG.log(Level.INFO, "POST {0}", baseUrl);
            RestConnection rc1 = new RestConnection(baseUrl);

            RestResponse res = rc1.get(headers);
            String responseBody = res.getDataAsString();

            LOG.log(Level.INFO, "got response: {0}", responseBody);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(responseBody);
            if (!json.get("OrderStatus__c").toString().equals("Pending")) {
                return json.get("License_Key__c").toString();
            } else {
                return "Pending";
            }

        } catch (Exception e) {
            LOG.log(Level.SEVERE, "error retreiving license-key", e.getMessage());
        }
        return null;

    }

    /**
     * Create Request to update license
     * <p>
     * Call to Salesforce description:<br>
     * <strong>URL:</strong><code> $oAuthinstanceURL$/services/data/v29.0/sobjects/OperatorOrder__c/</code><br>
     * <strong>Method: </strong>POST<br>
     * <strong>Input Parameters</strong><br><ul>
     * <li><strong>OperatorCustomerId__c: </strong><code>$BSK$</code></li>
     * <li><strong>License_Order_Type__c: </strong><code>"Get Updated License"</code> static</li>
     * <li><strong>BillingType__c: </strong><code>"Usage Based"</code></li>
     * <li><strong>MI_ANYWARE_FOUNDATION__c: </strong><code>"true"</code> always true/li>
     * <li><strong>MI_ANYWARE_Apps__c: </strong><code>true/false</code> if applicable in prod license</li>
     * <li><strong>MI_ANYWARE_Content__c: </strong><code>true/false</code> if applicable in prod license</li>
     * <li><strong>MI_ANYWARE_Security__c: </strong><code>true/false</code> if applicable in prod license</li>
     * </ul>
     * <br>
     * <strong>Request Headers</strong><br><ul>
     * <li><strong>Accept: </strong><code>"application/json"</code></li>
     * <li><strong>Authorization: </strong><code> "Bearer " + $accessToken$</code></li>
     * </ul>
     *
     * @param myTenant
     * @return String containing the license Id
     * @throws Exception
     */
    //TODO: new licence type
    public String createOperatorOrderForUpdateLicense(Tenant myTenant) throws Exception {

        try {

            String baseUrl = this.instanceUrl + operatorOrderUrl;

            //Setting request headers with authentication token
            String[][] headers = new String[][]{
                {"Authorization", "Bearer " + this.accessToken},
                {"Content-Type", "application/json"}
            };

            LOG.log(Level.INFO, "POST {0}", baseUrl);
            RestConnection rc1 = new RestConnection(baseUrl);

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("OperatorCustomerId__c", myTenant.getBsk());
            //Commented, not needed MobileIronUniqueId, only onb MI's own anyware cluster
            //jsonObject.put("MobileIronUniqueId__c", myTenant.getLicenceID());

            // OrderType Information
            jsonObject.put("License_Order_Type__c", "Get Updated License");
            jsonObject.put("BillingType__c", "Usage Based");
            //jsonObject.put("Billing_Term__c", "12");

            // License Info
            if (myTenant.getIsTrial() == false) {
                // OrderType Information for paying licence
                jsonObject.put("MI_ANYWARE_FOUNDATION__c", true);
                jsonObject.put("MI_ANYWARE_Apps__c", myTenant.getMianywareapps());
                jsonObject.put("MI_ANYWARE_Content__c", myTenant.getMianywarecontent());
                jsonObject.put("MI_ANYWARE_Security__c", myTenant.getMianywaresecurity());
            } else {
                // OrderType Information for trial license
                jsonObject.put("EVAL_MI_ANYWARE_FOUNDATION__c", true);
            }

            RestResponse res = rc1.post(headers, jsonObject.toJSONString());
            LOG.log(Level.INFO, "requesting licence update:{0}", jsonObject.toJSONString());

            String responseBody = res.getDataAsString();
            LOG.log(Level.INFO, "got response: {0}", responseBody);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(responseBody);

            if ((Boolean) json.get("success")) {
                String sret = (String) json.get("id");
                LOG.log(Level.INFO, "obtained id: {0}", sret);
                return sret;
            }

        } catch (Exception e) {
            LOG.log(Level.SEVERE, "error placing update licence request", e.getMessage());
        }
        return null;

    }

    /**
     * Create Request to revoke customer license
     * <p>
     * Call to Salesforce description:<br>
     * <strong>URL:</strong><code> $oAuthinstanceURL$/services/data/v29.0/sobjects/OperatorOrder__c/</code><br>
     * <strong>Method: </strong>POST<br>
     * <strong>Input Parameters</strong><br><ul>
     * <li><strong>OperatorCustomerId__c: </strong><code>$BSK$</code></li>
     * <li><strong>License_Order_Type__c: </strong><code>"Revoke License"</code> static</li>
     * </ul>
     * <br>
     * <strong>Request Headers</strong><br><ul>
     * <li><strong>Accept: </strong><code>"application/json"</code></li>
     * <li><strong>Authorization: </strong><code> "Bearer " + $accessToken$</code></li>
     * </ul>
     * @param Tenant
     * @throws Exception
     */
    public boolean revokeCustomerLicence(Tenant myTenant) throws Exception {
        try {
            String baseUrl = this.instanceUrl + operatorOrderUrl;

            //Setting request headers with authentication token
            String[][] headers = new String[][]{
                {"Authorization", "Bearer " + this.accessToken},
                {"Content-Type", "application/json"}
            };

            LOG.log(Level.INFO, "POST {0}", baseUrl);
            RestConnection rc1 = new RestConnection(baseUrl);

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("OperatorCustomerId__c", myTenant.getBsk());
            // OrderType Information
            jsonObject.put("License_Order_Type__c", "Revoke License");

            RestResponse res = rc1.post(headers, jsonObject.toJSONString());
            LOG.log(Level.INFO, "requesting licence revocation:{0}", jsonObject.toJSONString());
            String responseBody = res.getDataAsString();
            LOG.log(Level.INFO, "got response: {0}", responseBody);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(responseBody);

            if ((Boolean) json.get("success")) {
                LOG.log(Level.INFO, "License revoked for customer {0}", myTenant.getBsk());
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            LOG.log(Level.SEVERE, "error revocating licence", e.getMessage());
            return false;
        }
    }
}
