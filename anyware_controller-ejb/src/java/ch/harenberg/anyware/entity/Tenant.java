/*
 * Copyright (C) 2013 Frank Harenberg <frank@harenberg.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ch.harenberg.anyware.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity Class representing a tenant as it is persisted in the database
 * <p>
 * Methods in class are getters and setters for attributes
 *
 * @author Frank Harenberg <frank@harenberg.ch>
 */
@Entity
@Table(name = "tenant")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tenant.findAll", query = "SELECT t FROM Tenant t"),
    @NamedQuery(name = "Tenant.findByIdtenant", query = "SELECT t FROM Tenant t WHERE t.idtenant = :idtenant"),
    @NamedQuery(name = "Tenant.findByBsk", query = "SELECT t FROM Tenant t WHERE t.bsk = :bsk"),
    @NamedQuery(name = "Tenant.findByCustomerName", query = "SELECT t FROM Tenant t WHERE t.customerName = :customerName"),
    @NamedQuery(name = "Tenant.findByCustomerMail", query = "SELECT t FROM Tenant t WHERE t.customerMail = :customerMail"),
    @NamedQuery(name = "Tenant.findByCustomerStreet", query = "SELECT t FROM Tenant t WHERE t.customerStreet = :customerStreet"),
    @NamedQuery(name = "Tenant.findByCustomerState", query = "SELECT t FROM Tenant t WHERE t.customerState = :customerState"),
    @NamedQuery(name = "Tenant.findByCustomerZIP", query = "SELECT t FROM Tenant t WHERE t.customerZIP = :customerZIP"),
    @NamedQuery(name = "Tenant.findByCustomerCountry", query = "SELECT t FROM Tenant t WHERE t.customerCountry = :customerCountry"),
    @NamedQuery(name = "Tenant.findByAdminFirstName", query = "SELECT t FROM Tenant t WHERE t.adminFirstName = :adminFirstName"),
    @NamedQuery(name = "Tenant.findByAdminLastName", query = "SELECT t FROM Tenant t WHERE t.adminLastName = :adminLastName"),
    @NamedQuery(name = "Tenant.findByAdminPassword", query = "SELECT t FROM Tenant t WHERE t.adminPassword = :adminPassword"),
    @NamedQuery(name = "Tenant.findByLicenceID", query = "SELECT t FROM Tenant t WHERE t.licenceID = :licenceID"),
    @NamedQuery(name = "Tenant.findByLicenceKey", query = "SELECT t FROM Tenant t WHERE t.licenceKey = :licenceKey"),
    @NamedQuery(name = "Tenant.findByLicenceStatus", query = "SELECT t FROM Tenant t WHERE t.licenceStatus = :licenceStatus"),
    @NamedQuery(name = "Tenant.findByTenantId", query = "SELECT t FROM Tenant t WHERE t.tenantId = :tenantId"),
    @NamedQuery(name = "Tenant.findByTenantDmPartitionId", query = "SELECT t FROM Tenant t WHERE t.tenantDmPartitionId = :tenantDmPartitionId"),
    @NamedQuery(name = "Tenant.findByTenantCmPartitionId", query = "SELECT t FROM Tenant t WHERE t.tenantCmPartitionId = :tenantCmPartitionId"),
    @NamedQuery(name = "Tenant.findBySCSAdminMail", query = "SELECT t FROM Tenant t WHERE t.sCSAdminMail = :sCSAdminMail"),
    @NamedQuery(name = "Tenant.findBySCSAdminPassword", query = "SELECT t FROM Tenant t WHERE t.sCSAdminPassword = :sCSAdminPassword"),
    @NamedQuery(name = "Tenant.findByAdminLang", query = "SELECT t FROM Tenant t WHERE t.adminLang = :adminLang"),
    @NamedQuery(name = "Tenant.findByCustomerDomain", query = "SELECT t FROM Tenant t WHERE t.customerDomain = :customerDomain"),
    @NamedQuery(name = "Tenant.findByCustomerCity", query = "SELECT t FROM Tenant t WHERE t.customerCity = :customerCity"),
    @NamedQuery(name = "Tenant.findByIsTrial", query = "SELECT t FROM Tenant t WHERE t.isTrial = :isTrial"),
    @NamedQuery(name = "Tenant.findByTrialStartDate", query = "SELECT t FROM Tenant t WHERE t.trialStartDate = :trialStartDate"),
    @NamedQuery(name = "Tenant.findByTrialEndDate", query = "SELECT t FROM Tenant t WHERE t.trialEndDate = :trialEndDate")})
public class Tenant implements Serializable {
    @Column(name = "isTrial")
    private Boolean isTrial;
    @Column(name = "TrialReminderDate")
    @Temporal(TemporalType.DATE)
    private Date trialReminderDate;
    @Size(max = 45)
    @Column(name = "ApiUser")
    private String apiUser;
    @Size(max = 45)
    @Column(name = "ApiPass")
    private String apiPass;
    @Size(max = 65)
    @Column(name = "CskContact")
    private String cskContact;
    @Size(max = 65)
    @Column(name = "SalesContact")
    private String salesContact;
    @Column(name = "device_count_active")
    private Integer deviceCountActive;
    @Column(name = "mdm_certificate_uploaded")
    private Boolean mdmCertificateUploaded;
    @Column(name = "device_count")
    private Integer deviceCount;
    @Column(name = "active_users")
    private Integer activeUsers;
    @Column(name = "MIANYWAREAPPS")
    private Boolean mianywareapps;
    @Column(name = "MIANYWARESECURITY")
    private Boolean mianywaresecurity;
    @Column(name = "MIANYWARECONTENT")
    private Boolean mianywarecontent;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtenant")
    private Integer idtenant;
    @Size(max = 45)
    @Column(name = "BSK")
    private String bsk;
    @Size(max = 45)
    @Column(name = "CustomerName")
    private String customerName;
    @Size(max = 45)
    @Column(name = "CustomerMail")
    private String customerMail;
    @Size(max = 45)
    @Column(name = "CustomerStreet")
    private String customerStreet;
    @Size(max = 45)
    @Column(name = "CustomerState")
    private String customerState;
    @Size(max = 45)
    @Column(name = "CustomerZIP")
    private String customerZIP;
    @Size(max = 45)
    @Column(name = "CustomerCountry")
    private String customerCountry;
    @Size(max = 45)
    @Column(name = "AdminFirstName")
    private String adminFirstName;
    @Size(max = 45)
    @Column(name = "AdminLastName")
    private String adminLastName;
    @Size(max = 45)
    @Column(name = "AdminPassword")
    private String adminPassword;
    @Size(max = 45)
    @Column(name = "LicenceID")
    private String licenceID;
    @Size(max = 45)
    @Column(name = "LicenceKey")
    private String licenceKey;
    @Size(max = 45)
    @Column(name = "LicenceStatus")
    private String licenceStatus;
    @Column(name = "TenantId")
    private Integer tenantId;
    @Column(name = "TenantDmPartitionId")
    private Integer tenantDmPartitionId;
    @Column(name = "TenantCmPartitionId")
    private Integer tenantCmPartitionId;
    @Size(max = 45)
    @Column(name = "SCSAdminMail")
    private String sCSAdminMail;
    @Size(max = 45)
    @Column(name = "SCSAdminPassword")
    private String sCSAdminPassword;
    @Size(max = 45)
    @Column(name = "AdminLang")
    private String adminLang;
    @Size(max = 45)
    @Column(name = "CustomerDomain")
    private String customerDomain;
    @Size(max = 45)
    @Column(name = "CustomerCity")
    private String customerCity;
    @Column(name = "TrialStartDate")
    @Temporal(TemporalType.DATE)
    private Date trialStartDate;
    @Column(name = "TrialEndDate")
    @Temporal(TemporalType.DATE)
    private Date trialEndDate;
    @Column(name="InvitationMailSentDate")
    @Temporal(TemporalType.DATE)
    private Date invitationMailSentDate;

    public Date getInvitationMailSentDate() {
        return invitationMailSentDate;
    }

    public void setInvitationMailSentDate(Date invitationMailSentDate) {
        this.invitationMailSentDate = invitationMailSentDate;
    }

    public Tenant() {
    }

    public Tenant(Integer idtenant) {
        this.idtenant = idtenant;
    }

    public Tenant(Integer idtenant, boolean isTrial) {
        this.idtenant = idtenant;
        this.isTrial = isTrial;
    }

    public Integer getIdtenant() {
        return idtenant;
    }

    public void setIdtenant(Integer idtenant) {
        this.idtenant = idtenant;
    }

    public String getBsk() {
        return bsk;
    }

    public void setBsk(String bsk) {
        this.bsk = bsk;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerMail() {
        return customerMail;
    }

    public void setCustomerMail(String customerMail) {
        this.customerMail = customerMail;
    }

    public String getCustomerStreet() {
        return customerStreet;
    }

    public void setCustomerStreet(String customerStreet) {
        this.customerStreet = customerStreet;
    }

    public String getCustomerState() {
        return customerState;
    }

    public void setCustomerState(String customerState) {
        this.customerState = customerState;
    }

    public String getCustomerZIP() {
        return customerZIP;
    }

    public void setCustomerZIP(String customerZIP) {
        this.customerZIP = customerZIP;
    }

    public String getCustomerCountry() {
        return customerCountry;
    }

    public void setCustomerCountry(String customerCountry) {
        this.customerCountry = customerCountry;
    }

    public String getAdminFirstName() {
        return adminFirstName;
    }

    public void setAdminFirstName(String adminFirstName) {
        this.adminFirstName = adminFirstName;
    }

    public String getAdminLastName() {
        return adminLastName;
    }

    public void setAdminLastName(String adminLastName) {
        this.adminLastName = adminLastName;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

    public String getLicenceID() {
        return licenceID;
    }

    public void setLicenceID(String licenceID) {
        this.licenceID = licenceID;
    }

    public String getLicenceKey() {
        return licenceKey;
    }

    public void setLicenceKey(String licenceKey) {
        this.licenceKey = licenceKey;
    }

    public String getLicenceStatus() {
        return licenceStatus;
    }

    public void setLicenceStatus(String licenceStatus) {
        this.licenceStatus = licenceStatus;
    }

    public Integer getTenantId() {
        return tenantId;
    }

    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    public Integer getTenantDmPartitionId() {
        return tenantDmPartitionId;
    }

    public void setTenantDmPartitionId(Integer tenantDmPartitionId) {
        this.tenantDmPartitionId = tenantDmPartitionId;
    }

    public Integer getTenantCmPartitionId() {
        return tenantCmPartitionId;
    }

    public void setTenantCmPartitionId(Integer tenantCmPartitionId) {
        this.tenantCmPartitionId = tenantCmPartitionId;
    }

    public String getSCSAdminMail() {
        return sCSAdminMail;
    }

    public void setSCSAdminMail(String sCSAdminMail) {
        this.sCSAdminMail = sCSAdminMail;
    }

    public String getSCSAdminPassword() {
        return sCSAdminPassword;
    }

    public void setSCSAdminPassword(String sCSAdminPassword) {
        this.sCSAdminPassword = sCSAdminPassword;
    }

    public String getAdminLang() {
        return adminLang;
    }

    public void setAdminLang(String adminLang) {
        this.adminLang = adminLang;
    }

    public String getCustomerDomain() {
        return customerDomain;
    }

    public void setCustomerDomain(String customerDomain) {
        this.customerDomain = customerDomain;
    }

    public String getCustomerCity() {
        return customerCity;
    }

    public void setCustomerCity(String customerCity) {
        this.customerCity = customerCity;
    }

    public Date getTrialStartDate() {
        return trialStartDate;
    }

    public void setTrialStartDate(Date trialStartDate) {
        this.trialStartDate = trialStartDate;
    }

    public Date getTrialEndDate() {
        return trialEndDate;
    }

    public void setTrialEndDate(Date trialEndDate) {
        this.trialEndDate = trialEndDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtenant != null ? idtenant.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tenant)) {
            return false;
        }
        Tenant other = (Tenant) object;
        if ((this.idtenant == null && other.idtenant != null) || (this.idtenant != null && !this.idtenant.equals(other.idtenant))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ch.harenberg.anyware.entity.Tenant[ idtenant=" + idtenant + " ]";
    }

    public Boolean getMianywareapps() {
        return mianywareapps;
    }

    public void setMianywareapps(Boolean mianywareapps) {
        this.mianywareapps = mianywareapps;
    }

    public Boolean getMianywaresecurity() {
        return mianywaresecurity;
    }

    public void setMianywaresecurity(Boolean mianywaresecurity) {
        this.mianywaresecurity = mianywaresecurity;
    }

    public Boolean getMianywarecontent() {
        return mianywarecontent;
    }

    public void setMianywarecontent(Boolean mianywarecontent) {
        this.mianywarecontent = mianywarecontent;
    }

    public Date getTrialReminderDate() {
        return trialReminderDate;
    }

    public void setTrialReminderDate(Date trialReminderDate) {
        this.trialReminderDate = trialReminderDate;
    }

    public String getApiUser() {
        return apiUser;
    }

    public void setApiUser(String apiUser) {
        this.apiUser = apiUser;
    }

    public String getApiPass() {
        return apiPass;
    }

    public void setApiPass(String apiPass) {
        this.apiPass = apiPass;
    }

    public String getCskContact() {
        return cskContact;
    }

    public void setCskContact(String cskContact) {
        this.cskContact = cskContact;
    }

    public String getSalesContact() {
        return salesContact;
    }

    public void setSalesContact(String salesContact) {
        this.salesContact = salesContact;
    }

    public Boolean getIsTrial() {
        return isTrial;
    }

    public void setIsTrial(Boolean isTrial) {
        this.isTrial = isTrial;
    }

    public Integer getDeviceCountActive() {
        return deviceCountActive;
    }

    public void setDeviceCountActive(Integer deviceCountActive) {
        this.deviceCountActive = deviceCountActive;
    }

    public Boolean getMdmCertificateUploaded() {
        return mdmCertificateUploaded;
    }

    public void setMdmCertificateUploaded(Boolean mdmCertificateUploaded) {
        this.mdmCertificateUploaded = mdmCertificateUploaded;
    }

    public Integer getDeviceCount() {
        return deviceCount;
    }

    public void setDeviceCount(Integer deviceCount) {
        this.deviceCount = deviceCount;
    }

    public Integer getActiveUsers() {
        return activeUsers;
    }

    public void setActiveUsers(Integer activeUsers) {
        this.activeUsers = activeUsers;
    }
    
}
