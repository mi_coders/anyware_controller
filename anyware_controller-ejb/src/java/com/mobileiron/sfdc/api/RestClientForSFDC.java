/**
 * Copyright 2013 Mobile Iron, Inc.
 * All rights reserved.
 */

package com.mobileiron.sfdc.api;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

//import net.sf.json.JSONArray;
//import net.sf.json.JSONSerializer;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.params.HttpParams;

import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;

public class RestClientForSFDC {

    // Variables to be populated after gaining the Access token.
    private String              accessToken  = null;
    private Map<String, String> oauthLoginResponse;
    private String              instanceUrl;

    // Constants to be used in this class.

    private static final String environment  = "https://test.salesforce.com";
    private static final String authUrl      = "/services/oauth2/token";
    // private static final String restUrl = "/services/data/v25.0/";
    private static final String queryUrl     = "/services/data/v25.0/query";

    private static final String sObjectUrl   = "/services/data/v25.0/sobjects";
    @SuppressWarnings("unused")
    private static final String searchUrl    = "/services/data/v25.0/search";
    @SuppressWarnings("unused")
    private static final String recentUrl    = "/services/data/v25.0/recent";

    private static final String customUrl = "/services/apexrest/OperatorOrder/";
    
    private static final String username     = "apiuser@swisscom.com";
    private static final String password     = "a9rcLiS2-Q_1lb@u";

    private static final String clientId     = "3MVG9pHRjzOBdkd8EoPaWXoKlZ4wKCp.Xy3EYHh4LRCoNOQDDRI1ppZegdUXHkplF6p_E33KNmr3ehaoDWIBM";
    private static final String clientSecret = "6375960923628748449";

    public static void main(String[] args) {
       //String objectId;
        try {

            RestClientForSFDC oc = new RestClientForSFDC();
            oc.oAuthSessionProvider();
         oc.getAllOperatorOrdersCustomAPI();
            //objectId = oc.createOperatorOrderForNewLicense();
            // oc.getAllOperatorOrders();
          //  oc.getOperatorOrderById(objectId);
           //oc.createOperatorOrderForProvisionCustomer();
            //oc.createOperatorOrderForUpdateCustomer();
          //  oc.createOperatorOrderForUpdateLicense();
            //oc.createOperatorOrderForRevokeCustomer();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    public void oAuthSessionProvider() throws HttpException, IOException {

        // Set up an HTTP client that makes a connection to REST API.
        DefaultHttpClient client = new DefaultHttpClient();
        org.apache.http.params.HttpParams params = client.getParams();
        params.setParameter(HttpConnectionParams.CONNECTION_TIMEOUT, 30000);

        // Set the SID.
        System.out.println("Logging in as " + username + " in environment " + environment);
        String baseUrl = environment + authUrl;

        // Send a post request to the OAuth URL.
        HttpPost oauthPost = new HttpPost(baseUrl);

        // The request body must contain these 5 values.
        List<BasicNameValuePair> parametersBody = new ArrayList<BasicNameValuePair>();
        parametersBody.add(new BasicNameValuePair("grant_type", "password"));
        parametersBody.add(new BasicNameValuePair("username", username));
        parametersBody.add(new BasicNameValuePair("password", password));
        parametersBody.add(new BasicNameValuePair("client_id", clientId));
        parametersBody.add(new BasicNameValuePair("client_secret", clientSecret));
        oauthPost.setEntity(new UrlEncodedFormEntity(parametersBody, HTTP.UTF_8));

        // Execute the request.
        System.out.println("POST " + baseUrl + "...\n");

        HttpResponse response = client.execute(oauthPost);
        int code = response.getStatusLine().getStatusCode();

        System.out.println("Code >> " + code);

        // populate Response map to fetch ACCESS TOKEN for any future call.
        this.oauthLoginResponse = (Map<String, String>)JSONValue.parse(EntityUtils.toString(response.getEntity()));

        System.out.println("OAuth login response");

        for (Map.Entry<String, String> entry : this.oauthLoginResponse.entrySet()) {
            System.out.println(String.format("  %s = %s", entry.getKey(), entry.getValue()));
        }

        // Populate Access Token
        this.accessToken = this.oauthLoginResponse.get("access_token");
        this.instanceUrl = this.oauthLoginResponse.get("instance_url");
        System.out.println("");
    }

    /**
     * Get all the Operator Orders from the MI Sales force Instance
     * 
     * @throws Exception
     */
    public void getAllOperatorOrders() throws Exception {

        try {

            DefaultHttpClient httpClient = new DefaultHttpClient();
            List<BasicNameValuePair> qsList = new ArrayList<BasicNameValuePair>();
            qsList.add(new BasicNameValuePair("q", "SELECT Name from OperatorOrder__c limit 10"));

            String queryString = URLEncodedUtils.format(qsList, "UTF-8");
            HttpGet get = new HttpGet(this.instanceUrl + queryUrl + "?" + queryString);
            get.setHeader("Authorization", "Bearer " + this.accessToken);

            System.out.println("GET " + this.instanceUrl + queryUrl + "?" + queryString + "...\n");
            HttpResponse queryResponse = httpClient.execute(get);

            System.out.println(queryResponse.getStatusLine().getReasonPhrase());
            Map<String, Object> querynfo = (Map<String, Object>)JSONValue.parse(EntityUtils.toString(queryResponse.getEntity()));

            System.out.println("Query response");
            for (Map.Entry<String, Object> entry : querynfo.entrySet()) {
                System.out.println(String.format("  %s = %s", entry.getKey(), entry.getValue()));
            }
            System.out.println("");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Get Operator Order by Id from the Salesforce Instance
     * 
     * @param objectId
     * @throws Exception
     */
    public void getOperatorOrderById(final String objectId) throws Exception {

        try {

            HttpClient httpclient = new HttpClient();
            String serviceurl = this.instanceUrl + sObjectUrl + "/OperatorOrder__c/" + objectId;
            GetMethod gm = new GetMethod(serviceurl);

            // set the token in the header
            gm.setRequestHeader("Authorization", "Bearer " + this.accessToken);

            NameValuePair[] params = new NameValuePair[1];

            params[0] = new NameValuePair("fields", "ID, Name, License_Key__c, OrderStatus__c");
            gm.setQueryString(params);

            httpclient.executeMethod(gm);
            String responseBody = gm.getResponseBodyAsString();
            System.out.println(responseBody);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Send the request for the New License
     * 
     * @throws Exception
     */
    @SuppressWarnings({ "unchecked", "deprecation" })
    public String createOperatorOrderForNewLicense() throws Exception {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Calendar cal = Calendar.getInstance();
            HttpClient httpclient = new HttpClient();
            String serviceurl = this.instanceUrl + sObjectUrl + "/OperatorOrder__c/";
            PostMethod pm = new PostMethod(serviceurl);

            // set the token in the header
            pm.setRequestHeader("Authorization", "Bearer " + this.accessToken);
            pm.setRequestHeader("Content-Type", "application/json");

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("OperatorCustomerId__c", "UniqueId-009789");

            // Customer Account Information
            jsonObject.put("Customer_Name__c", "Acme Inc 2");

            jsonObject.put("Customer_Street__c", "123 some creay stree");
            jsonObject.put("Customer_City__c", "Mountain View");
            jsonObject.put("Customer_State__c", "CA");
            jsonObject.put("Customer_Country__c", "America");
            jsonObject.put("Customer_Zip_Code__c", "94043");

            // Tenant Admin for Customer
            jsonObject.put("Admin_First_Name__c", "Joe");
            jsonObject.put("Admin_Last_Name__c", "Moesby");
            jsonObject.put("Admin_Email_ID__c", "sample1@sample.com");
            jsonObject.put("Domain__c", "sample.com");
            jsonObject.put("Admin_Password__c", "myPa$$W0rd");

            // OrderType Information
            jsonObject.put("License_Order_Type__c", "Get new License");
            jsonObject.put("BillingType__c", "Usage Based");
            jsonObject.put("Billing_Term__c", "12");

            // License Info
            jsonObject.put("NumberOfDevices__c", 20.00);
            jsonObject.put("MI_ANYWARE_FOUNDATION__c", true);
            jsonObject.put("StartDate__c", formatter.format(cal.getTime()));
            cal.add(Calendar.DAY_OF_MONTH, 365);
            jsonObject.put("EndDate__c", formatter.format(cal.getTime()));

            pm.setRequestBody(jsonObject.toJSONString());

            httpclient.executeMethod(pm);

            String responseBody = pm.getResponseBodyAsString();
            System.out.println(responseBody);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject)parser.parse(responseBody);

            if ((Boolean)json.get("success")) {
                return (String)json.get("id");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    
    
    
    @SuppressWarnings({ "unchecked", "deprecation" })
    public String createOperatorOrderForProvisionCustomer() throws Exception {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Calendar cal = Calendar.getInstance();
            HttpClient httpclient = new HttpClient();
            String serviceurl = this.instanceUrl + sObjectUrl + "/OperatorOrder__c/";
            PostMethod pm = new PostMethod(serviceurl);

            // set the token in the header
            pm.setRequestHeader("Authorization", "Bearer " + this.accessToken);
            pm.setRequestHeader("Content-Type", "application/json");

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("OperatorCustomerId__c", "UniqueId-009789");

            // Customer Account Information
            jsonObject.put("Customer_Name__c", "Acme Inc 2");

            jsonObject.put("Customer_Street__c", "123 some creay stree");
            jsonObject.put("Customer_City__c", "Mountain View");
            jsonObject.put("Customer_State__c", "CA");
            jsonObject.put("Customer_Country__c", "America");
            jsonObject.put("Customer_Zip_Code__c", "94043");

            // Tenant Admin for Customer
            jsonObject.put("Admin_First_Name__c", "Joe");
            jsonObject.put("Admin_Last_Name__c", "Moesby");
            jsonObject.put("Admin_Email_ID__c", "sample1@sample.com");
            jsonObject.put("Domain__c", "sample.com");
            jsonObject.put("Admin_Password__c", "myPa$$W0rd");

            // OrderType Information
            jsonObject.put("Order_Type__c", "Provision Customer");
            jsonObject.put("BillingType__c", "Usage Based");
            jsonObject.put("Billing_Term__c", "12");

            // License Info
            jsonObject.put("NumberOfDevices__c", 20.00);
            jsonObject.put("MI_ANYWARE_FOUNDATION__c", true);
            jsonObject.put("StartDate__c", formatter.format(cal.getTime()));
            cal.add(Calendar.DAY_OF_MONTH, 365);
            jsonObject.put("EndDate__c", formatter.format(cal.getTime()));

            pm.setRequestBody(jsonObject.toJSONString());

            httpclient.executeMethod(pm);

            String responseBody = pm.getResponseBodyAsString();
            System.out.println(responseBody);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject)parser.parse(responseBody);

            if ((Boolean)json.get("success")) {
                return (String)json.get("id");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    
    
    @SuppressWarnings({ "unchecked", "deprecation" })
    public String createOperatorOrderForUpdateCustomer() throws Exception {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Calendar cal = Calendar.getInstance();
            HttpClient httpclient = new HttpClient();
            String serviceurl = this.instanceUrl + sObjectUrl + "/OperatorOrder__c/";
            PostMethod pm = new PostMethod(serviceurl);

            // set the token in the header
            pm.setRequestHeader("Authorization", "Bearer " + this.accessToken);
            pm.setRequestHeader("Content-Type", "application/json");

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("OperatorCustomerId__c", "UniqueId-009789");
            jsonObject.put("MobileIronUniqueId__c", "Mobileiron generated ID for a customer");

        
            // OrderType Information
            jsonObject.put("Order_Type__c", "Update Customer");
            jsonObject.put("BillingType__c", "Usage Based");
            jsonObject.put("Billing_Term__c", "12");

            
            // License Info
            jsonObject.put("NumberOfDevices__c", 20.00);
            jsonObject.put("MI_ANYWARE_FOUNDATION__c", true);
            jsonObject.put("StartDate__c", formatter.format(cal.getTime()));
            cal.add(Calendar.DAY_OF_MONTH, 365);
            jsonObject.put("EndDate__c", formatter.format(cal.getTime()));

            
            pm.setRequestBody(jsonObject.toJSONString());

            httpclient.executeMethod(pm);

            String responseBody = pm.getResponseBodyAsString();
            System.out.println(responseBody);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject)parser.parse(responseBody);

            if ((Boolean)json.get("success")) {
                return (String)json.get("id");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    
    @SuppressWarnings({ "unchecked", "deprecation" })
    public String createOperatorOrderForUpdateLicense() throws Exception {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Calendar cal = Calendar.getInstance();
            HttpClient httpclient = new HttpClient();
            String serviceurl = this.instanceUrl + sObjectUrl + "/OperatorOrder__c/";
            PostMethod pm = new PostMethod(serviceurl);

            // set the token in the header
            pm.setRequestHeader("Authorization", "Bearer " + this.accessToken);
            pm.setRequestHeader("Content-Type", "application/json");

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("OperatorCustomerId__c", "UniqueId-009789");
            jsonObject.put("MobileIronUniqueId__c", "Mobileiron generated ID for a customer");

        
            // OrderType Information
            jsonObject.put("License_Order_Type__c", "Update License");
            jsonObject.put("BillingType__c", "Usage Based");
            jsonObject.put("Billing_Term__c", "12");

            
            // License Info
            jsonObject.put("NumberOfDevices__c", 20.00);
            jsonObject.put("MI_ANYWARE_FOUNDATION__c", true);
            jsonObject.put("StartDate__c", formatter.format(cal.getTime()));
            cal.add(Calendar.DAY_OF_MONTH, 365);
            jsonObject.put("EndDate__c", formatter.format(cal.getTime()));

            
            pm.setRequestBody(jsonObject.toJSONString());

            httpclient.executeMethod(pm);

            String responseBody = pm.getResponseBodyAsString();
            System.out.println(responseBody);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject)parser.parse(responseBody);

            if ((Boolean)json.get("success")) {
                return (String)json.get("id");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    
    
    @SuppressWarnings({ "unchecked", "deprecation" })
    public String createOperatorOrderForRevokeCustomer() throws Exception {

     
        try {
         
            HttpClient httpclient = new HttpClient();
            String serviceurl = this.instanceUrl + sObjectUrl + "/OperatorOrder__c/";
            PostMethod pm = new PostMethod(serviceurl);

            // set the token in the header
            pm.setRequestHeader("Authorization", "Bearer " + this.accessToken);
            pm.setRequestHeader("Content-Type", "application/json");

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("OperatorCustomerId__c", "UniqueId-009789");
            jsonObject.put("MobileIronUniqueId__c", "Mobileiron generated ID for a customer");
            jsonObject.put("Order_Type__c", "Revoke Customer");
  
            pm.setRequestBody(jsonObject.toJSONString());

            httpclient.executeMethod(pm);

            String responseBody = pm.getResponseBodyAsString();
            System.out.println(responseBody);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject)parser.parse(responseBody);

            if ((Boolean)json.get("success")) {
                return (String)json.get("id");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    
    public void getAllOperatorOrdersCustomAPI() throws Exception {

       try {
              HttpClient httpclient = new HttpClient();
            String serviceurl = this.instanceUrl + customUrl;
            GetMethod gm = new GetMethod(serviceurl);
    
            // set the token in the header
            gm.setRequestHeader("Authorization", "Bearer " + this.accessToken);
    
            NameValuePair[] params = new NameValuePair[1];
    
            params[0] = new NameValuePair("fields", "ID, Name, License_Key__c, OrderStatus__c");
            gm.setQueryString(params);
    
     
            httpclient.executeMethod(gm);
            String responseBody = gm.getResponseBodyAsString();
            System.out.println(responseBody);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    
    
    @SuppressWarnings({ "unchecked", "deprecation" })
    public void getAllOperatorOrdersCustomAPI2() throws Exception {

        try {
               HttpClient httpclient = new HttpClient();
             String serviceurl = this.instanceUrl + customUrl;
             PostMethod pm = new PostMethod(serviceurl);
     
             // set the token in the header
             pm.setRequestHeader("Authorization", "Bearer " + this.accessToken);
     
             NameValuePair[] params = new NameValuePair[1];
     
             params[0] = new NameValuePair("fields", "ID, Name, License_Key__c, OrderStatus__c");
             pm.setQueryString(params);
             JSONObject jsonObject = new JSONObject();

             jsonObject.put("OperatorCustomerId__c", "UniqueId-009789");
             jsonObject.put("MobileIronUniqueId__c", "Mobileiron generated ID for a customer");
             jsonObject.put("Order_Type__c", "Revoke Customer");
   
             pm.setRequestBody(jsonObject.toJSONString());

      
             httpclient.executeMethod(pm);
             String responseBody = pm.getResponseBodyAsString();
             System.out.println(responseBody);

         } catch (Exception e) {
             e.printStackTrace();
         }

     }
    
    
    
}
