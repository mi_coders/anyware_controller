/*
 * Copyright (C) 2014 fhadmin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ch.harenberg.anyware.remoteSystems;

import ch.harenberg.anyware.entity.Tenant;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author fhadmin
 */
public class RestClientForSFDCTest {
    
    public RestClientForSFDCTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createOperatorOrderForNewLicense method, of class RestClientForSFDC.
     */
//    @Test
//    public void testCreateOperatorOrderForNewLicense() throws Exception {
//        System.out.println("createOperatorOrderForNewLicense");
//        SecureRandom random = new SecureRandom();
//        String BSK = new BigInteger(130, random).toString(32);
//
//        Tenant myTenant = new Tenant();
//        myTenant.setCustomerName("SCSTest" + BSK);
//        myTenant.setCustomerStreet("SCS Street");
//        myTenant.setCustomerCity("SomePlace");
//        myTenant.setCustomerState("Ticino");
//        myTenant.setCustomerCountry("Confederatio Helveticae");
//        myTenant.setCustomerZIP("7070");
//        myTenant.setAdminFirstName("CoC");
//        myTenant.setAdminLastName("Dataservices");
//        myTenant.setBsk(BSK);
//        myTenant.setAdminPassword("FooBar444");
//        myTenant.setCustomerDomain("foobar.li");
//        myTenant.setSCSAdminMail(BSK + ".mds@swisscom.com");
//        myTenant.setIsTrial(false);
//        RestClientForSFDC instance = new RestClientForSFDC();
//        String result = instance.createOperatorOrderForNewLicense(myTenant);
//        System.out.println(result);    
//        assertNotNull(result);
//    }

    /**
     * Test of getOperatorOrderById method, of class RestClientForSFDC.
     */

//    @Test
//    public void testGetOperatorOrderById() throws Exception {
//        System.out.println("getOperatorOrderById");
//        Tenant myTenant = new Tenant();
//        
//        myTenant.setLicenceID("a0OQ0000006A6gTMAS");
//        RestClientForSFDC instance = new RestClientForSFDC();
//        String result = instance.getOperatorOrderById(myTenant);
//        System.out.println(result);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of createOperatorOrderForUpdateLicense method, of class RestClientForSFDC.
//     */
    @Test
    public void testCreateOperatorOrderForUpdateLicense() throws Exception {
        System.out.println("createOperatorOrderForUpdateLicense");
        Tenant myTenant = new Tenant();
        myTenant.setBsk("h9rcrgafilcibr13hb9v90c00t");
        RestClientForSFDC instance = new RestClientForSFDC();
        String result = instance.createOperatorOrderForUpdateLicense(myTenant);
        System.out.println(result);
        assertNotNull(result);
    }

    /**
     * Test of revokeCustomerLicence method, of class RestClientForSFDC.
     */
//    @Test
//    public void testRevokeCustomerLicence() throws Exception {
//        System.out.println("revokeCustomerLicence");
//        Tenant Tenant = null;
//        RestClientForSFDC instance = new RestClientForSFDC();
//        instance.revokeCustomerLicence(Tenant);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
}
