/*
 * Copyright (C) 2013 Frank Harenberg, frank@harenberg.ch
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ch.harenberg.anyware.remoteSystems;

import ch.harenberg.anyware.entity.Tenant;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import java.security.SecureRandom;
import java.math.BigInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Frank Harenberg, frank@harenberg.ch
 */
public class RestClientForAnywareTest {
    public Tenant myTenant = new Tenant();
    public Integer AdminUserId=10705;
    public Integer firstRoleId=12011;
    
    public RestClientForAnywareTest() {
        SecureRandom random = new SecureRandom();
        String BSK = new BigInteger(130, random).toString(32);
        Integer bsk = 22470 ;
        //String BSK;
        //BSK = bsk.toString();

        myTenant.setCustomerName("Balteschwiler AG");
        myTenant.setAdminFirstName("Boris");
        myTenant.setAdminLastName("Brenner");
        myTenant.setAdminLang("de");
        myTenant.setBsk(BSK);
        myTenant.setSCSAdminPassword("Mi4man11");
        myTenant.setCustomerDomain("frama.com");
        myTenant.setSCSAdminMail("marco.vento@gmail.com");
        myTenant.setLicenceKey("bee6befd-0977-4f07-9223-e55298e28770");
        myTenant.setCustomerMail(BSK+"test@karingelo.ch");
        myTenant.setAdminPassword("WaeS4BimavudaPh2");
        myTenant.setTenantCmPartitionId(20001);
        myTenant.setTenantDmPartitionId(23001);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
    /**
     * Test of createTenant method, of class RestClientForAnyware.
     *
     */
//    @Test
//    public void testCreateTenant() throws Exception {
//        
//        RestClientForAnyware instance = new RestClientForAnyware();
//        Tenant result = instance.createTenant(myTenant);
//        System.out.println("createTenant ID (" + result.getTenantId()+")");
//        assertNotNull(result.getTenantId());
//        myTenant.setTenantId(result.getIdtenant());
//        myTenant.setTenantCmPartitionId(result.getTenantCmPartitionId());
//        myTenant.setTenantDmPartitionId(result.getTenantDmPartitionId());
//    }
//    
//     /**
//     * Test of applyLicence method, of class RestClientForAnyware.
//     */
//    @Test
//    public void testApplyLicence() {
//        System.out.println("applyLicence");
//        RestClientForAnyware instance = new RestClientForAnyware();
//        instance.applyLicence(myTenant);
//    }
//
//    /**
//     * Test of acceptEulaForTenant method, of class RestClientForAnyware.
//     */
//    @Test
//    public void testAcceptEulaForTenant() {
//        System.out.println("acceptEulaForTenant");
//        RestClientForAnyware instance = new RestClientForAnyware();
//        instance.acceptEulaForTenant(myTenant);
//    }

    /**
     * Test of createTenantSuperUser method, of class RestClientForAnyware.
     */
//    @Test
//    public void testCreateTenantSuperUser() {
//        System.out.println("createTenantSuperUser");
//        RestClientForAnyware instance = new RestClientForAnyware();
//        Integer result = instance.createTenantSuperUser(myTenant);
//        assertNotNull(result);
//        AdminUserId=result;
//        System.out.println("TenantUserId: "+result);
//    }
    
//    @Test
//    public void testCreateTenantAllTasks() {
//   //     try {
//            //Create Tenant
//            RestClientForAnyware instance = new RestClientForAnyware();
////            Tenant result = instance.createTenant(myTenant);
////            System.out.println("createTenant ID (" + result.getTenantId()+")");
////            myTenant.setTenantId(result.getIdtenant());
////            myTenant.setTenantCmPartitionId(result.getTenantCmPartitionId());
////            myTenant.setTenantDmPartitionId(result.getTenantDmPartitionId());
////            //Apply Licence
////            instance.applyLicence(myTenant);
////            //Accept Licence
////            instance.acceptEulaForTenant(myTenant);
////            //Create Tenant Super User
////            System.out.println("createTenantSuperUser");
////            Integer AdminUserId = instance.createTenantSuperUser(myTenant);
////            System.out.println("TenantUserId: "+result);
//            //Get Roles Roles
//            int firstRoleId = instance.getFirstTenantRole(myTenant);
//            //Assign Roles
//            instance.assignCustomerAdminRoles(myTenant, AdminUserId, firstRoleId);
////        } catch (IOException ex) {
////            Logger.getLogger(RestClientForAnywareTest.class.getName()).log(Level.SEVERE, null, ex);
////        } catch (ParseException ex) {
////            Logger.getLogger(RestClientForAnywareTest.class.getName()).log(Level.SEVERE, null, ex);
////        }
//    }

    /**
     * Test of getFirstTenantRole method, of class RestClientForAnyware.
     */
    @Test
    public void testGetFirstTenantRole() {
        System.out.println("getFirstTenantRole");
        RestClientForAnyware instance = new RestClientForAnyware();
        int result = instance.getFirstTenantRole(myTenant);
        assertNotNull(result);
        firstRoleId = result;
        System.out.println("FirstTenantRole: "+result);
    }

    /**
     * Test of assignCustomerAdminRoles method, of class RestClientForAnyware.
     */
    @Test
    public void testAssignCustomerAdminRoles() {
        try {
            System.out.println("assignCustomerAdminRoles");
            RestClientForAnyware instance = new RestClientForAnyware();
            instance.assignCustomerAdminRoles(myTenant, AdminUserId, firstRoleId);
        } catch (IOException ex) {
            Logger.getLogger(RestClientForAnywareTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    /**
     * Test of getPlatformReport method, of class RestClientForAnyware.
     */
    @Test
    public void testGetPlatformReport() {
        System.out.println("getPlatformReport");
        RestClientForAnyware instance = new RestClientForAnyware();
        JSONArray result = instance.getPlatformReport();
        assertNotNull(result);
    }

   

}
